require("dotenv").config();
const {Telegraf} = require('telegraf');
const {
    Extra,
    Markup,
    Stage,
    session,
    Scenes,
    Context,
} = require('telegraf');
const randomPhoto = 'https://picsum.photos/200/300/?random';
const bot = new Telegraf(process.env.BOT_TOKEN);

// подключение сцены
const SceneGenerator = require('telegraf');
const curScene = new Scenes();
const ageScene = curScene.GenAgeScene();
const nameScene = curScene.GennameScene();

const stage = new Scenes.Stage([
    ageScene,
    nameScene
]);
bot.use(session());
bot.use(stage.middleware());
// при вводе команды выводит инфу о сесси
bot.hears("info", async (ctx) => console.log(ctx));

bot.start(async (ctx) => {
    ctx.reply(
        `Вітаю! 🙂 \n\nОберіть необхідну дію в меню і я із задоволенням вам доможу.\n\n Наберіть /help для отримання команд Оберіть дію:`
    );
});
bot.help((ctx) => ctx.reply("Send me a sticker"));
bot.on("sticker", (ctx) =>
    ctx.replyWithSticker(
        "CAACAgIAAxkBAANYYDeQtCC-Js7xvw9ogbnb_gsg7UwAAvUBAAImRekDE58wRFl4WBgeBA"
    )
);
bot.hears("Привет", (ctx) =>
    ctx.reply(`Добро пожаловать ${ctx.message.from.first_name}!`)
);
bot.command('scene', async (ctx) => {
    ctx.scene.enter('age')
})
bot.command('cat', (ctx) => ctx.replyWithPhoto(randomPhoto));
bot.command('cat2', (ctx) => ctx.replyWithPhoto({url: randomPhoto}));
bot.command("test", (ctx) =>
    ctx.reply("test", {
        reply_markup: {
            resize_keyboard: true,
            keyboard: [[{text: "Авторизация"}]],
        },
    })
);
bot.hears('Авторизация', 'ctx');
// Логер отправляет в терминал любое дейтсвие
bot.use(Telegraf.log());

bot.launch();

console.log('Bot Srated');

process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
