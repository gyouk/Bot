// exports.Scenes = require("./scenes");
const {Telegraf, Context} = require('telegraf');
const { Scenes } = require('telegraf');


class SceneGenerator {
    GenAgeScene() {
        const age = new Scenes.BaseScene('age');
        age.enter(async (ctx) => {
            await ctx.reply('Привет! Ты вошел в сцену возраста. Укажи его')
        });
        age.on('text', async (ctx) => {
            const currAge = Number(ctx.message.text);
            if (currAge && currAge > 0) {
                await ctx.reply('Спасибо за возраст!!');
                ctx.scene.enter('name')
            } else {
                await ctx.reply('Меня не проведешь! Напиши пожалуйста возраст цифрами и больше нуля');
                ctx.scene.reenter()
            }
        });
        age.on('message', (ctx) => ctx.reply('Давай лучше возраст'));
        return age
    };

    GennameScene() {
        const name = new Scenes.BaseScene('name');
        name.enter((ctx) => ctx.reply('теперь ты в сцене имени.Представься'));
        name.on('text', async (ctx) => {
            const name = ctx.message.text;
            if (name) {
                await ctx.reply(`Greating ${name}`);
            } else {
                await ctx.reply('я так и не понял, как тебя зовут');
                await ctx.scene.reenter()
            }
        });
        name.on('messege', (ctx) => ctx.reply('это явно  не твое имя'));
        return name;
    }
};
module.exports = SceneGenerator;